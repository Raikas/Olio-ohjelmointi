﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee
{
    public class Employee
    {
        int id = 1;
        string firstName, lastName;
        int salary = 2500;
        int annualSalary;

        public int getID()
        {
            return id;
        }

        public string FirstName()
        {
            Console.Write("First name: ");
            firstName = Console.ReadLine();
            return firstName;
        }

        public string LastName()
        {
            Console.Write("Last name: ");
            lastName = Console.ReadLine();
            return lastName;
        }

        public string Name()
        {
            return "Firstname: " + firstName + "\n" + "Lastname: " + lastName + "\n";

        }
        public int getSalary()
        {
            return salary;
        }
        public void setSalary(int newSalary)
        {
            salary = newSalary;
        }

        public int AnnualSalary()
        {
            annualSalary = salary * 12;
            return annualSalary;
        }

        public int raiseSalary(int percent)
        {
            salary = salary + percent;
            return salary;
        }

        public string toString()
        {
            return "id: " + getID() + "\n" + Name() + "salary: " + getSalary() + "\n" + "annual salary: " + AnnualSalary();
        }
    }



    public class Program
    {
        static void Main(string[] args)
        {
            Employee e1 = new Employee();

            e1.FirstName();
            e1.LastName();
            Console.WriteLine();
            Console.WriteLine(e1.toString());
            Console.WriteLine();

            Employee e2 = new Employee();

            e2.FirstName();
            e2.LastName();
            e2.setSalary(4000);
            e2.raiseSalary(500);

            Console.WriteLine();
            Console.WriteLine(e2.toString());
        }
    }
}
