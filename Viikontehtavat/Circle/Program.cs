﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circle
{

    public class Circle { 
    
        private double radius;
        private string color;
    

    public Circle()
    {
        radius = 1.0;
        color = "blue";
    }

    public void Circle2(double _radius)
    {
        radius = _radius;
        color = "blue";
    }

    public double getRadius()
    {
        return radius;
    }

    public double getArea()
    {
        return radius * radius * Math.PI;
    }

    public string getColor()
    {
        return color;
    }
    }


    public class Program
    {
        static void Main(string[] args)
        {

            Circle c1 = new Circle();
            c1.Circle2(50);
            Console.WriteLine("The circle has radius of " + c1.getRadius() + ", an area of " + c1.getArea() + " and it's " + c1.getColor());
        }
    }
}
