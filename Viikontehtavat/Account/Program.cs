﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account
{
    public class Account
    {
        public string id;
        public string name;
        public int balance = 0;

        public Account()
        {
            id = "ID";
            name = "Name";
        }

        public void _Account(string _id, string _name, int _balance)
        {
            id = _id;
            name = _name;
            balance = _balance;
        }

        public string getID()
        {
            return id;
        }

        public string getName()
        {
            return name;
        }

        public int getBalance()
        {
            return balance;
        }

        public void setBalance(int newBalance)
        {
            balance = newBalance;
        }

        public int credit(int amount)
        {
           return amount + balance;
        }

        public int debit(int amount)
        {
            if (amount <= balance)
            {
                balance = balance - amount;
            }
            else
            {
               Console.WriteLine("Amount exceeded balance");
            }

            return balance;
        }

        public int transferTo(Account another, int amount)
        {
            if (amount <= balance)
            {
                another.balance += amount;
            }
            else
            {
                Console.WriteLine("Amount exceeded balance");
            }

            return another.balance;
        }

        public string toString()
        {
            return "Account" + "\n" + "id: " + id + "\n" + "Name: " + name + "\n" + "Balance: " + balance;
        }

    }
    public class Program
    {
        static void Main(string[] args)
        {
            Account a1 = new Account();

            Console.WriteLine(a1.toString());
            Console.WriteLine();

            Account a2 = new Account();

            a2.setBalance(1000);
            a2.balance = a2.credit(500);
            a2.debit(800);
            a2.transferTo(a2, 500);
            

            Console.WriteLine(a2.toString());
        }
    }
}
