﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rectangle
{
    public class Rectangle
    {
        private float length, width;

        public Rectangle()
        {
            length = 1.0f;
            width = 1.0f;
        }

        public float getLength()
        {
            return length;
        }

        public void setLength(float newLength)
        {
            length = newLength;
        }

        public float getWidth()
        {
            return width;
        }

        public void setWidth(float newWidth)
        {
            width = newWidth;
        }

        public double getArea()
        {
           return length * width;
        }

        public double getPerimeter()
        {
            return length + length + width + width;
        }

        public string toString()
        {
            return "Rectangle [area=" + getArea() + " perimeter=" + getPerimeter() + "]";
        }
    }



    public class Program
    {
        static void Main(string[] args)
        {
            Rectangle r1 = new Rectangle();

            Console.WriteLine(r1.toString());

            Rectangle r2 = new Rectangle();
            r2.setWidth(5);
            r2.setLength(10);

            Console.WriteLine(r2.toString());


        }
    }
}
