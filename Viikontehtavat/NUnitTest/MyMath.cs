﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTest
{
    public class MyMath
    {
        public int Add(int a,int b)
        {
            return a + b*3;
        }

        public int Sub(int a, int b)
        {
            return a - b;
        }
    }
}
