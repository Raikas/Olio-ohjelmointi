﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Date
{
    public class Date
    {
        public int myDay, myYear, myMonth;
        public List<int> day = new List<int>();
        public List<int> month = new List<int>();
        public List<int> year = new List<int>();
        public int userDay;
        public int userMonth;
        public int userYear;

        public Date(int _day, int _month, int _year)
        {
            myDay = _day;
            myMonth = _month;
            myYear = _year;
        }

        public Date()
        {
        }

        public int getDay()
        {
            Console.Write("Day: ");
            string s;
            s = Console.ReadLine();
            int.TryParse(s, out userDay);
            return userDay;
        }

        public int getMonth()
        {
            Console.Write("Month: ");
            string s;
            s = Console.ReadLine();
            int.TryParse(s, out userMonth);
            return userMonth;
        }

        public int getYear()
        {
            Console.Write("Year: ");
            string s;
            s = Console.ReadLine();
            int.TryParse(s, out userYear);
            return userYear;
        }

        public void setDay(int newDay)
        {
            myDay = newDay;
        }

        public void setMonth(int newMonth)
        {
            myMonth = newMonth;
        }

        public void setYear(int newYear)
        {
            myYear = newYear;
        }

        public void setDate(int day, int month, int year)
        {
            myDay = day;
            myMonth = month;
            myYear = year;
        }

        public string toString()
        {
            return userDay + "/" + userMonth + "/" + userYear .ToString();
        }

        public void Init(Date d)
        {
            for (int i = 1; i <= 31; i++)
            {
                d.day.Add(i);
            }
            for (int i = 1; i <= 12; i++)
            {
                d.month.Add(i);
            }
            for (int i = 1900; i <= 9999; i++)
            {
                d.year.Add(i);
            }
        }
    }
    public class Program
    {
        static void Main()
        {
            Date d1 = new Date();
            d1.getDay();
            d1.getMonth();
            d1.getYear();
            d1.Init(d1);
           

                if (d1.day.Contains(d1.userDay) && d1.month.Contains(d1.userMonth) && d1.year.Contains(d1.userYear))
                {
                    Console.WriteLine(d1.toString());
                }
                else
                {
                   Console.WriteLine("Invalid date, month or year");
                   Main();
                }
        }

    }
}
