﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invoiceitem
{
    public class InvoiceItem
    {
        string id;
        string desc;
        int qty;
        double unitPrice;


        public InvoiceItem()
        {
            id = "Item";
            desc = "Test item description";
            qty = 1;
            unitPrice = 1;
        }

        public string getID()
        {
            Console.Write("Item ID: ");
            id = Console.ReadLine();
            return id;
        }

        public string getDesc()
        {
            Console.Write("Item description: ");
            desc = Console.ReadLine();
            return desc;
        }

        public int getQty()
        {
            return qty;
        }

        public void setQty(int newQty)
        {
            qty = newQty;
        }

        public double getUnitPrice()
        {
            return unitPrice;
        }

        public void setUnitPrice(double newUnitPrice)
        {
            unitPrice = newUnitPrice;
        }

        public double getTotal()
        {
            return unitPrice * qty;
        }

        public string toString()
        {
            return "InvoiceItem \n\nid: " + id + "\n" + "Description: " + desc + "\n" + "Quantity: " + qty + "\n" + "Unit Price: " + unitPrice + "\n" + "Total:" + getTotal();
        }
    }


    public class Program
    {
        static void Main(string[] args)
        {
            InvoiceItem item1 = new InvoiceItem();

            item1.getID();
            item1.getDesc();

            Console.WriteLine(item1.toString());
            Console.WriteLine();

            InvoiceItem item2 = new InvoiceItem();

            item2.getID();
            item2.getDesc();

            item2.setQty(5);
            item2.setUnitPrice(15);

            Console.WriteLine(item2.toString());
        }
    }
}
