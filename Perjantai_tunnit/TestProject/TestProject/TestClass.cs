﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    [TestFixture]
    class TestClass
    {
        [TestCase]
        public void Summa()
        {
            Program_class p1 = new Program_class();
            Assert.AreEqual(20, p1.Summa(10, 10));
        }

        [TestCase]
        public void Kertoma()
        {
            Program_class p1 = new Program_class();
            Assert.AreEqual(50, p1.Kertoma(10, 5));
        }
    }
}
