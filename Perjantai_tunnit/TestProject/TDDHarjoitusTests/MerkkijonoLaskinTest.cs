﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDDHarjoitusTests
{
    [TestFixture]
    public class MerkkijonoLaskinTest
    {
        MerkkijonoLaskin laskin;

        [SetUp]
        public void TestienAlustus()
        {
            laskin = new MerkkijonoLaskin();
        }

        [Test]
        public void TyhjaMerkkijonoPalauttaaNollan()
        {
            Assert.That(laskin.Summa(""), Is.EqualTo(0));
        }

        [Test]
        public void YksiAnnettuLukuPalauttaaLuvunArvon()
        { 
            Assert.That(laskin.Summa("5"), Is.EqualTo(5));
        }

        [Test]
        public void KaksiLukuaPilkullaErotettunaPalauttaaSumman()
        {
            Assert.That(laskin.Summa("1,2"), Is.EqualTo(3));
        }

        [Test]
        public void UseampiKuinKaksiLukuaPilkullaErotettunaPalauttaaSumman()
        {
            Assert.That(laskin.Summa("1,2,3,4,5"), Is.EqualTo(15));
        }
    }
}
